import json
import logging
import os
import re
import requests
import sys
import random

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.file_detector import LocalFileDetector
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from time import sleep
from typing import Dict, List

""" CONFIG """

#setting file config
path_config = "config.json"
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

CHROMEDRIVER = config['setting'][0]['chromedriver']
ULANG = config['setting'][0]["jumlah_perulangan"]
JEDA = config['setting'][0]["jeda"]
AKUN = config['setting'][0]["akun"]
TOKEN = config['setting'][0]["telegram_bot_token"]
CHATID = config['setting'][0]["telegram_chat_id"]
FILE_DOWNLOAD = config['scrape_produk'][0]["file"]
FILE_DOWNLOAD_SEPARATOR = config['scrape_produk'][0]["separator"]
FILE_UPLOAD = config['update_produk'][0]["file"]
FILE_UPLOAD_SEPARATOR = config['update_produk'][0]["separator"]
START_UPDATE = config['update_produk'][0]["start"]
JEDA_UPDATE = config['update_produk'][0]["jeda"]
FILE_UPLOAD_PRODUK = config['upload_produk'][0]["file"]
LAPORAN_UPLOAD_PRODUK = config['upload_produk'][0]["laporan_berhasil"]
LAPORAN_UPLOAD_PRODUK_GAGAL = config['upload_produk'][0]["laporan_gagal"]
START_UPLOAD_PRODUK = config['upload_produk'][0]["start"]
JEDA_UPLOAD_PRODUK = config['upload_produk'][0]["jeda"]
FILE_DELETE = config['delete_produk'][0]["file"]
FILE_OPTIMASI_PESAN = config['optimasi_pesan'][0]["file"]
FILE_OPTIMASI_SEPARATOR = config['update_produk'][0]["separator"]
START_OPTIMASI_PESAN = config['optimasi_pesan'][0]["start"]
JEDA_OPTIMASI_PESAN = config['optimasi_pesan'][0]["jeda"]
RANDOM_PESAN = config['optimasi_pesan'][0]["pesan"]

def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat_baru.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)

def laporan_upload(data):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{};{}".format(dt_string,data)
    f = open(LAPORAN_UPLOAD_PRODUK, "a")
    f.write(tulisan+"\n")
    f.close()

def laporan_upload_gagal(data):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{};{}".format(dt_string,data)
    f = open(LAPORAN_UPLOAD_PRODUK_GAGAL, "a")
    f.write(tulisan+"\n")
    f.close()

""" Login module """

def domain_to_url(domain: str) -> str:
    """ Converts a (partial) domain to valid URL """
    if domain.startswith("."):
        domain = "www" + domain
    return "http://" + domain

def login_using_cookie_file(driver: WebDriver, cookie_file: str):
    """Restore auth cookies from a file. Does not guarantee that the user is logged in afterwards.
    Visits the domains specified in the cookies to set them, the previous page is not restored."""
    domain_cookies: Dict[str, List[object]] = {}
    with open(cookie_file) as file:
        cookies: List = json.load(file)
        # Sort cookies by domain, because we need to visit to domain to add cookies
        for cookie in cookies:
            try:
                domain_cookies[cookie["domain"]].append(cookie)
            except KeyError:
                domain_cookies[cookie["domain"]] = [cookie]

    for domain, cookies in domain_cookies.items():
        driver.get(domain_to_url(domain + "/robots.txt"))
        for cookie in cookies:
            cookie.pop("sameSite", None)  # Attribute should be available in Selenium >4
            cookie.pop("storeId", None)  # Firefox container attribute
            try:
                driver.add_cookie(cookie)
            except:
                tulis_log(f"Couldn't set cookie {cookie['name']} for {domain}")

def upload_produk(driver: WebDriver,nm_kategori,id_kategori,judul,harga,keterangan,lokasi,gambar):
    driver.get("https://mobile.facebook.com/marketplace/selling/item/")

    #judul
    tulis_log("Set judul produk : {}".format(judul))
    driver.find_element(By.NAME, "title").click()
    driver.find_element(By.NAME, "title").send_keys(judul)
    sleep(random.randrange(3, 10))

    #harga
    tulis_log("Set harga produk : {}".format(harga))
    driver.find_element(By.NAME, "price").click()
    driver.find_element(By.NAME, "price").send_keys(harga)
    sleep(random.randrange(3, 10))

    #deskripsi
    tulis_log("Set deskripsi")
    driver.find_element(By.NAME, "description").click()
    driver.find_element(By.NAME, "description").send_keys(keterangan)
    sleep(random.randrange(3, 10))

    #kategori
    tulis_log("Set kategori : {}".format(nm_kategori))
    WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, "//div[2]/div/div/div[4]/div/div"))).click()
    WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, "//div[@id=\'modalDialog\']/div[@id=\'modalDialogView\']/div/div/div[{}]/div".format(id_kategori)))).click()
    sleep(random.randrange(3, 10))
    
    #lokasi
    tulis_log("Set lokasi : {}".format(lokasi))
    WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, "//form/div[2]/div/div/div[5]/div/div"))).click()
    WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, "//div[2]/div/div/div/div/input"))).click()
    WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, "//div[2]/div/div/div/div/input"))).send_keys(lokasi)
    WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, "//div[@id=\'nt-typeahead-results-view\']/div[@id=\'0\']/div/div"))).click()
    sleep(random.randrange(3, 10))
    
    #upload foto
    xn = 0
    for x in gambar:
        xn = xn+1
        tulis_log("Upload gambar ke-{}".format(xn))
        driver.find_element(By.XPATH, "//input[@type=\"file\"]").send_keys(x)
        sleep(random.randrange(3, 10))
    
    #tombol posting
    WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, "//div[starts-with(@id,'u_0_25_')]"))).click()
    sleep(10)
    

file = open(FILE_UPLOAD_PRODUK, "r")
my_data = file.readlines()[START_UPLOAD_PRODUK:]
file.close()

for line in my_data:
    d = line.split(";")
    nomor = d[0]
    cookies = d[1]
    nm_kategori = d[2]
    id_kategori = d[3]
    judul = d[4]
    harga = d[5]
    keterangan = d[6].replace("<br>","\n")
    kota = d[7]
    label = d[8].split("|")
    gambar = []
    for i in d[9:18]:
        i = i.replace("\n","")
        if i != "":
            gambar.append(i)
            
    tulis_log("Upload produk ke {}".format(nomor))
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications" : 2}
    chrome_options.add_experimental_option("prefs",prefs)
    driver = webdriver.Chrome(r"chromedriver.exe",chrome_options=chrome_options)
    
    try:
        tulis_log("Login ke akun -> {}".format(cookies))
        login_using_cookie_file(driver,cookies)
        driver.get("https://mobile.facebook.com")
        sleep(5)
    
        tulis_log("Proses upload")
        upload_produk(driver,nm_kategori,id_kategori,judul,harga,keterangan,kota,gambar)
        
        tulis_log("Berhasil Upload -> {};{};{};{}".format(cookies,judul,kota,driver.current_url))
        laporan_upload("{};{};{};{};{}".format(nomor, cookies, judul, kota, "Berhasil (via mobile)"))
        sleep(random.randrange(JEDA_UPLOAD_PRODUK - 10, JEDA_UPLOAD_PRODUK + 10))
        driver.close()
        sleep(random.randrange(5, 10))
        tulis_log("==================")
        
    except Exception as e:
        laporan_upload_gagal("{};{};{};{};{}".format(nomor,cookies, judul, kota, "Gagal (via mobile)"))
        tulis_log("Error saat proses ke {}".format(nomor))
        tulis_log(e)
        tulis_log("==================")
        driver.close()
    
tulis_log('Done !!!')