import json
import logging
import os
import re
import requests
import sys
import time
import sqlite3
import random

import urllib.parse
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.file_detector import LocalFileDetector
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.wait import WebDriverWait
from time import sleep
from typing import Dict, List

""" CONFIG """

#setting file config
path_config = "config.json"
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

CHROMEDRIVER = config['setting'][0]['chromedriver']
ULANG = config['setting'][0]["jumlah_perulangan"]
JEDA = config['setting'][0]["jeda"]
AKUN = config['setting'][0]["akun"]
TOKEN = config['setting'][0]["telegram_bot_token"]
CHATID = config['setting'][0]["telegram_chat_id"]
FILE_DOWNLOAD = config['scrape_produk'][0]["file"]
FILE_DOWNLOAD_SEPARATOR = config['scrape_produk'][0]["separator"]
FILE_UPLOAD = config['update_produk'][0]["file"]
FILE_UPLOAD_SEPARATOR = config['update_produk'][0]["separator"]
START_UPDATE = config['update_produk'][0]["start"]
JEDA_UPDATE = config['update_produk'][0]["jeda"]
FILE_UPLOAD_PRODUK = config['upload_produk'][0]["file"]
START_UPLOAD_PRODUK = config['upload_produk'][0]["start"]
JEDA_UPLOAD_PRODUK = config['upload_produk'][0]["jeda"]
FILE_DELETE = config['delete_produk'][0]["file"]
FILE_OPTIMASI_PESAN = config['optimasi_pesan'][0]["file"]
FILE_OPTIMASI_SEPARATOR = config['update_produk'][0]["separator"]
START_OPTIMASI_PESAN = config['optimasi_pesan'][0]["start"]
JEDA_OPTIMASI_PESAN = config['optimasi_pesan'][0]["jeda"]
RANDOM_PESAN = config['optimasi_pesan'][0]["pesan"]

""" FUNGSI TULIS"""
def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)

""" Login module """

def domain_to_url(domain: str) -> str:
    """ Converts a (partial) domain to valid URL """
    if domain.startswith("."):
        domain = "www" + domain
    return "http://" + domain

def login_using_cookie_file(driver: WebDriver, cookie_file: str):
    """Restore auth cookies from a file. Does not guarantee that the user is logged in afterwards.
    Visits the domains specified in the cookies to set them, the previous page is not restored."""
    domain_cookies: Dict[str, List[object]] = {}
    with open(cookie_file) as file:
        cookies: List = json.load(file)
        # Sort cookies by domain, because we need to visit to domain to add cookies
        for cookie in cookies:
            try:
                domain_cookies[cookie["domain"]].append(cookie)
            except KeyError:
                domain_cookies[cookie["domain"]] = [cookie]

    for domain, cookies in domain_cookies.items():
        driver.get(domain_to_url(domain + "/robots.txt"))
        for cookie in cookies:
            cookie.pop("sameSite", None)  # Attribute should be available in Selenium >4
            cookie.pop("storeId", None)  # Firefox container attribute
            try:
                driver.add_cookie(cookie)
            except:
                tulis_log(f"Couldn't set cookie {cookie['name']} for {domain}")
                
def update_judul(driver:WebDriver):
    judul = WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.NAME, "title"))).get_attribute("value")
    if "." in judul:
        judul = judul.replace(".","")
    else:
        judul = "{}.".format(judul)

    driver.find_element(By.NAME, "title").click()
    driver.find_element(By.NAME, "title").send_keys(Keys.CONTROL + "a")
    driver.find_element(By.NAME, "title").send_keys(Keys.DELETE)
    time.sleep(random.randrange(1, 3))
    driver.find_element(By.NAME, "title").send_keys(judul)
    time.sleep(random.randrange(3, 10))

    WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, "//div[starts-with(@id,'u_0_20_')]"))).click()
    sleep(10)
    return judul

# kodeakun = "#ZX001"
# cookies = "F:\FB_MP\Cookies FB\#ZX001.json"
# nama = "Claudia Audy"

f = open(FILE_UPLOAD, "r")
a = f.readlines()[START_UPDATE-1:]
f.close()

#     kodeakun = "#ZX001"
#     cookies = "F:\FB_MP\Cookies FB\#ZX001.json"
#     nama = "Claudia Audy"

count = START_UPDATE
for line in a:
    data = line.strip().split(FILE_UPLOAD_SEPARATOR)
    kodeakun = data[0]
    nama = data[1]
    cookies = data[2]
    url = data[3]
    judul = data[4]
    id_produk = re.findall(r'\d+', url)[0]
    
    # chrome_options = Options()
    # chrome_options.add_argument("--headless")
    # driver = webdriver.Chrome(CHROMEDRIVER,options=chrome_options)
    # driver = webdriver.Chrome(CHROMEDRIVER)
    chrome_options = webdriver.ChromeOptions()
    prefs = {"profile.default_content_setting_values.notifications" : 2}
    chrome_options.add_experimental_option("prefs",prefs)
    # chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(CHROMEDRIVER,chrome_options=chrome_options)
    
    try:
        tulis_log("Proses ke {} - update produk untuk akun {} | {}".format(count,kodeakun,nama))
        tulis_log("Login akun")
        login_using_cookie_file(driver,cookies)
        tulis_log("Buka halaman produk")
        driver.get("https://mobile.facebook.com/marketplace/selling/item/?for_sale_item_id={}".format(id_produk))
        time.sleep(10)
        tulis_log("Judul lama -> {}".format(judul))
#         judul_baru = update_judul(driver)
        judul = driver.find_element(By.NAME, "title").get_attribute("value")
        if "." in judul:
            judul = judul.replace(".","")
        else:
            judul = "{}.".format(judul)

        driver.find_element(By.NAME, "title").click()
        driver.find_element(By.NAME, "title").send_keys(Keys.CONTROL + "a")
        driver.find_element(By.NAME, "title").send_keys(Keys.DELETE)
        time.sleep(random.randrange(1, 3))
        driver.find_element(By.NAME, "title").send_keys(judul)
        time.sleep(random.randrange(3, 10))
        tulis_log("Judul baru -> {}".format(judul))

        WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH, "//div[starts-with(@id,'u_0_20_')]"))).click()
        sleep(20)
        driver.close()
        tulis_log("Berhasil update produk")
        tulis_log("Jeda {} detik".format(JEDA_UPDATE))
        time.sleep(JEDA_UPDATE)
    except Exception as e:
        tulis_log("Error '{}' saat proses ke {}".format(e,count))
        driver.close()
        
    count += 1
tulis_log("Done !!!")