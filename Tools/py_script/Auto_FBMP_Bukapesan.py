import json
import os
import requests
import sys
import time

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.wait import WebDriverWait
from time import sleep
from typing import Dict, List

""" CONFIG """

#setting file config
path_config = "config.json"
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

CHROMEDRIVER = config['setting'][0]['chromedriver']
ULANG = config['setting'][0]["jumlah_perulangan"]
JEDA = config['setting'][0]["jeda"]
AKUN = config['setting'][0]["akun"]
TOKEN = config['setting'][0]["telegram_bot_token"]
CHATID = config['setting'][0]["telegram_chat_id"]
FILE_DOWNLOAD = config['scrape_produk'][0]["file"]
FILE_DOWNLOAD_SEPARATOR = config['scrape_produk'][0]["separator"]
FILE_UPLOAD = config['update_produk'][0]["file"]
FILE_UPLOAD_SEPARATOR = config['update_produk'][0]["separator"]
START_UPDATE = config['update_produk'][0]["start"]
JEDA_UPDATE = config['update_produk'][0]["jeda"]
FILE_UPLOAD_PRODUK = config['upload_produk'][0]["file"]
START_UPLOAD_PRODUK = config['upload_produk'][0]["start"]
JEDA_UPLOAD_PRODUK = config['upload_produk'][0]["jeda"]
FILE_DELETE = config['delete_produk'][0]["file"]
FILE_OPTIMASI_PESAN = config['optimasi_pesan'][0]["file"]
FILE_OPTIMASI_SEPARATOR = config['update_produk'][0]["separator"]
START_OPTIMASI_PESAN = config['optimasi_pesan'][0]["start"]
JEDA_OPTIMASI_PESAN = config['optimasi_pesan'][0]["jeda"]
RANDOM_PESAN = config['optimasi_pesan'][0]["pesan"]


""" FUNGSI TULIS"""
def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)

""" Telegram Send Message """

def susun_text(data):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    text = "**Laporan Pesan masuk pada tanggal {}**\n".format(dt_string)
    for i in data:
        dt = i.split(";")
        kode_akun = dt[0]
        nama_akun = dt[1]
        jumlah_pesan = dt[2]
        new_line = "{} | {} -> {} Pesan Baru".format(kode_akun,nama_akun,jumlah_pesan)
        text = "{}\n{}".format(text,new_line)
    return text.strip()

def telegram_bot_sendtext(bot_message):
    bot_token = TOKEN
    bot_chatID = CHATID
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message
    response = requests.get(send_text)
    res = response.json()
    return res['ok']

""" Login module """

def domain_to_url(domain: str) -> str:
    """ Converts a (partial) domain to valid URL """
    if domain.startswith("."):
        domain = "www" + domain
    return "http://" + domain

def login_using_cookie_file(driver: WebDriver, cookie_file: str):
    """Restore auth cookies from a file. Does not guarantee that the user is logged in afterwards.
    Visits the domains specified in the cookies to set them, the previous page is not restored."""
    domain_cookies: Dict[str, List[object]] = {}
    with open(cookie_file) as file:
        cookies: List = json.load(file)
        # Sort cookies by domain, because we need to visit to domain to add cookies
        for cookie in cookies:
            try:
                domain_cookies[cookie["domain"]].append(cookie)
            except KeyError:
                domain_cookies[cookie["domain"]] = [cookie]

    for domain, cookies in domain_cookies.items():
        driver.get(domain_to_url(domain + "/robots.txt"))
        for cookie in cookies:
            cookie.pop("sameSite", None)  # Attribute should be available in Selenium >4
            cookie.pop("storeId", None)  # Firefox container attribute
            try:
                driver.add_cookie(cookie)
            except:
                tulis_log(f"Couldn't set cookie {cookie['name']} for {domain}")

""" Fungsi Cek Pesan FB """
def tombol_lanjut():
    try:
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//a[contains(.,\'Lihat Pesan Sebelumnya\')]"))).click()
        sleep(2)
        tombol_lanjut()
    except:
        time.sleep(1)
        
def buka_pesan(driver: WebDriver,set_cookies):
    login_using_cookie_file(driver,set_cookies)
    driver.get("https://mobile.facebook.com")
    driver.get("https://mobile.facebook.com/messages/?folder=unread")
    try:
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//a[contains(.,\'Lihat Pesan Sebelumnya\')]"))).click()
        sleep(2)
        for i in range(1,10):
          WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//a[contains(.,\'Lihat Pesan Sebelumnya\')]"))).click()  
    except:
        time.sleep(1)
    
def tutup_pesan(driver: WebDriver):
    print("\nKetik 1 untuk menutup pesan : ")
    n = int(input())
    if n == 1:
        driver.close()
        utama()
    else:
        print("Perintah anda salah !!!")
        tutup_pesan(driver)
        

def buka_akun(data):
    print("\nSilahkan masukan nomor akun yang mau dibuka :")
    try:
        nomorakun = int(input())
        dt = data[nomorakun-1].split(";")
        kodeakun = dt[0]
        cookies = dt[1]
        nama = dt[2]
        driver = webdriver.Chrome(CHROMEDRIVER)
        buka_pesan(driver,cookies)
        tutup_pesan(driver)
    except:
        print("Anda salah memasukan nomor akun !!!")
        buka_akun(data)

def utama():
    f = open(AKUN, "r")
    data_login = f.readlines()[1:]
    f.close()

    text1 = """ ==== Data Akun ===="""
    count = 0
    for a in data_login:
        count += 1
        data = a.strip().split(";")
        kodeakun = data[0]
        cookies = data[1]
        nama = data[2]
        text1 = text1 + ("\n{} | {} -> {}".format(count,kodeakun,nama))

    print(text1.strip())
    buka_akun(data_login)

utama()