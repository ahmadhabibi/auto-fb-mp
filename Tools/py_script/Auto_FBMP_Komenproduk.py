import json
import logging
import os
import re
import requests
import sys
import time
import sqlite3
import random

import urllib.parse
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.file_detector import LocalFileDetector
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.wait import WebDriverWait
from time import sleep
from typing import Dict, List

""" CONFIG """

#setting file config
path_config = "config.json"
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

CHROMEDRIVER = config['setting'][0]['chromedriver']
AKUN = config['komen_produk_orang'][0]["akun"]
RANDOM_PESAN = config['komen_produk_orang'][0]["pesan"]
JEDA = config['komen_produk_orang'][0]["jeda"]
TOTAL = config['komen_produk_orang'][0]["total_optimasi_perakun"]

""" FUNGSI TULIS"""
def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)
    
def simpan(text):
    f = open("scrape_produk.csv", "a")
    f.write(text+"\n")
    f.close()

""" Login module """

def domain_to_url(domain: str) -> str:
    """ Converts a (partial) domain to valid URL """
    if domain.startswith("."):
        domain = "www" + domain
    return "http://" + domain

def login_using_cookie_file(driver: WebDriver, cookie_file: str):
    """Restore auth cookies from a file. Does not guarantee that the user is logged in afterwards.
    Visits the domains specified in the cookies to set them, the previous page is not restored."""
    domain_cookies: Dict[str, List[object]] = {}
    with open(cookie_file) as file:
        cookies: List = json.load(file)
        # Sort cookies by domain, because we need to visit to domain to add cookies
        for cookie in cookies:
            try:
                domain_cookies[cookie["domain"]].append(cookie)
            except KeyError:
                domain_cookies[cookie["domain"]] = [cookie]

    for domain, cookies in domain_cookies.items():
        driver.get(domain_to_url(domain + "/robots.txt"))
        for cookie in cookies:
            cookie.pop("sameSite", None)  # Attribute should be available in Selenium >4
            cookie.pop("storeId", None)  # Firefox container attribute
            try:
                driver.add_cookie(cookie)
            except:
                tulis_log(f"Couldn't set cookie {cookie['name']} for {domain}")

def search_fbmp(driver):
    kw = random.choice(open("keyword_fbmp.txt","r").read().splitlines())
    driver.get("https://web.facebook.com/marketplace")
    bahasa = "Telusuri Marketplace"
    try:
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,'(//input[@aria-label="{}"])[1]'.format(bahasa))))
    except:
        bahasa = "Search Marketplace"
    driver.find_element(By.XPATH,'(//input[@aria-label="{}"])[1]'.format(bahasa)).click()
    sleep(3)
    driver.find_element(By.XPATH,'(//input[@aria-label="{}"])[1]'.format(bahasa)).send_keys(Keys.CONTROL + "A")
    sleep(1)
    driver.find_element(By.XPATH,'(//input[@aria-label="{}"])[1]'.format(bahasa)).send_keys(Keys.BACKSPACE)
    sleep(1)
    tulis_log("Cari produk : {}".format(kw))
    driver.find_element(By.XPATH,'(//input[@aria-label="{}"])[1]'.format(bahasa)).send_keys(kw)
    sleep(1)
    driver.find_element(By.XPATH,'(//input[@aria-label="{}"])[1]'.format(bahasa)).send_keys(Keys.RETURN)
    sleep(5)

    for y in range(1,6):
        scroll(y)

    WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//a[contains(@href,\'/marketplace/item\')]")))
    produk = driver.find_elements(By.XPATH, "//a[contains(@href,\'/marketplace/item\')]")
    jumlah_produk = len(produk)
    pilih_produk = random.randint(1, jumlah_produk)
    tulis_log("Pilih produk nomor {} dari total {} produk".format(pilih_produk,jumlah_produk))
    produk[pilih_produk].click()
    sleep(10)

    try:
        #klik tombol pesan
        WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, '//div[@class=\'oi9244e8 buofh1pr\']/div[@class=\'oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 pq6dq46d p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l cbu4d94t taijpn5t k4urcfbm\']')))
        driver.find_element(By.XPATH, '//div[@class=\'oi9244e8 buofh1pr\']/div[@class=\'oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 pq6dq46d p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l cbu4d94t taijpn5t k4urcfbm\']').click()

        #isi teks area
        pesan = random.choice(RANDOM_PESAN)
        WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, '//textarea[contains(@id,\'jsc_c\')]')))
        driver.find_element(By.XPATH, '//textarea[contains(@id,\'jsc_c\')]').click()
        driver.find_element(By.XPATH, '//textarea[contains(@id,\'jsc_c\')]').send_keys(pesan)
        sleep(2)

        #kirim
        WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, '//div[@class=\'dati1w0a hv4rvrfc ihqw7lf3 bkfpd7mw j83agx80\']/div[2]/div')))
        driver.find_element(By.XPATH, '//div[@class=\'dati1w0a hv4rvrfc ihqw7lf3 bkfpd7mw j83agx80\']/div[2]/div').click()
        tulis_log("Berhasil kirim pesan")
    except:
        tulis_log("Sudah terkirim sebelumnya")

def scroll(i):
    driver.execute_script("window.scrollTo(0, document.getElementById('facebook').scrollHeight);")
    sleep(2)

f = open(AKUN, "r")
akuns = f.readlines()[1:]
f.close()

count = 0
for akun in akuns:
    count += 1
    data = akun.strip().split(";")
    kodeakun = data[0]
    cookies = data[1]
    nama = data[2]
    
    # tulis_log("Proses optimasi akun {} | {}".format(kodeakun,nama))
    # driver = webdriver.Chrome(CHROMEDRIVER)
    # login_using_cookie_file(driver,cookies)
    # driver.get("https://mobile.facebook.com")

    try:
        tulis_log("Proses optimasi akun {} | {}".format(kodeakun, nama))
        for x in range(1, TOTAL+1):
            tulis_log("Proses ke - {}".format(x))
            chrome_options = webdriver.ChromeOptions()
            prefs = {"profile.default_content_setting_values.notifications": 2}
            chrome_options.add_experimental_option("prefs", prefs)
            driver = webdriver.Chrome(CHROMEDRIVER, chrome_options=chrome_options)
            login_using_cookie_file(driver, cookies)
            driver.get("https://mobile.facebook.com")

            search_fbmp(driver)
            jedanya = random.randrange(JEDA-5,JEDA+5)
            tulis_log("Jeda {} detik".format(jedanya))
            sleep(jedanya)
            driver.close()
    except Exception as e:
        tulis_log("Proses error untuk akun {} | {}".format(kodeakun,nama))
        tulis_log(e)
        driver.close()

tulis_log("Done")