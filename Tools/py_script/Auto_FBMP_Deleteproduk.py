import json
import logging
import os
import re
import requests
import sys
import time
import sqlite3

import urllib.parse
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.file_detector import LocalFileDetector
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.wait import WebDriverWait
from time import sleep
from typing import Dict, List

""" CONFIG """

#setting file config
path_config = "config.json"
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")


CHROMEDRIVER = config['setting'][0]['chromedriver']
ULANG = config['setting'][0]["jumlah_perulangan"]
JEDA = config['setting'][0]["jeda"]
AKUN = config['setting'][0]["akun"]
TOKEN = config['setting'][0]["telegram_bot_token"]
CHATID = config['setting'][0]["telegram_chat_id"]
FILE_DOWNLOAD = config['scrape_produk'][0]["file"]
FILE_DOWNLOAD_SEPARATOR = config['scrape_produk'][0]["separator"]
FILE_UPLOAD = config['update_produk'][0]["file"]
FILE_UPLOAD_SEPARATOR = config['update_produk'][0]["separator"]
START_UPDATE = config['update_produk'][0]["start"]
JEDA_UPDATE = config['update_produk'][0]["jeda"]
FILE_UPLOAD_PRODUK = config['upload_produk'][0]["file"]
START_UPLOAD_PRODUK = config['upload_produk'][0]["start"]
JEDA_UPLOAD_PRODUK = config['upload_produk'][0]["jeda"]
FILE_AKUN_DELETE = config['delete_produk'][0]["file"]
START_DELETE = config['delete_produk'][0]["start"]
VIEW_MINIMAL_DELETE = config['delete_produk'][0]["view_minimal"]
FILE_OPTIMASI_PESAN = config['optimasi_pesan'][0]["file"]
FILE_OPTIMASI_SEPARATOR = config['update_produk'][0]["separator"]
START_OPTIMASI_PESAN = config['optimasi_pesan'][0]["start"]
JEDA_OPTIMASI_PESAN = config['optimasi_pesan'][0]["jeda"]
RANDOM_PESAN = config['optimasi_pesan'][0]["pesan"]

""" FUNGSI TULIS"""
def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)
    
def simpan(text):
    f = open("scrape_produk.csv", "a")
    f.write(text+"\n")
    f.close()

""" Login module """

def domain_to_url(domain: str) -> str:
    """ Converts a (partial) domain to valid URL """
    if domain.startswith("."):
        domain = "www" + domain
    return "http://" + domain

def login_using_cookie_file(driver: WebDriver, cookie_file: str):
    """Restore auth cookies from a file. Does not guarantee that the user is logged in afterwards.
    Visits the domains specified in the cookies to set them, the previous page is not restored."""
    domain_cookies: Dict[str, List[object]] = {}
    with open(cookie_file) as file:
        cookies: List = json.load(file)
        # Sort cookies by domain, because we need to visit to domain to add cookies
        for cookie in cookies:
            try:
                domain_cookies[cookie["domain"]].append(cookie)
            except KeyError:
                domain_cookies[cookie["domain"]] = [cookie]

    for domain, cookies in domain_cookies.items():
        driver.get(domain_to_url(domain + "/robots.txt"))
        for cookie in cookies:
            cookie.pop("sameSite", None)  # Attribute should be available in Selenium >4
            cookie.pop("storeId", None)  # Firefox container attribute
            try:
                driver.add_cookie(cookie)
            except:
                tulis_log(f"Couldn't set cookie {cookie['name']} for {domain}")


f = open(FILE_AKUN_DELETE, "r")
lines = f.readlines()[1:]
f.close()

#     kodeakun = "#ZX001"
#     cookies = "F:\FB_MP\Cookies FB\#ZX001.json"
#     nama = "Claudia Audy"

count = 0
for line in lines:
    data = line.strip().split(";")
    kodeakun = data[0]
    cookies = data[1]
    nama = data[2]

    try:
        tulis_log("Proses hapus produk akun {} | {}".format(kodeakun, nama))

        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        chrome_options.add_experimental_option("prefs", prefs)
        driver = webdriver.Chrome(CHROMEDRIVER, chrome_options=chrome_options)
        login_using_cookie_file(driver, cookies)
        driver.get("https://mobile.facebook.com")
        tulis_log("Buka halaman produk")
        driver.get("https://www.facebook.com/marketplace/you/selling")
        time.sleep(5)

        # SCROLL
        tulis_log("Hitung produk")
        lastHeight = driver.execute_script("return document.documentElement.scrollHeight")
        while True:
            driver.execute_script("window.scrollTo(0, document.documentElement.scrollHeight);")
            time.sleep(2)
            newHeight = driver.execute_script("return document.documentElement.scrollHeight")
            if newHeight == lastHeight:
                break
            lastHeight = newHeight
        time.sleep(5)

        # HITUNG PRODUK
        jumlah_produk = driver.find_elements(By.XPATH, "//div[@class=\'sonix8o1\']/span/div[@class=\'jktsbyx5 n851cfcs\']/div[@class=\'cwj9ozl2 ue3kfks5 pw54ja7n uo3d90p7 l82x9zwi o16s864r sej5wr8e m8hsej2k k4urcfbm rnsnyeob\']")
        jumlah_produk = len(jumlah_produk)
        tulis_log("Ditemukan {} produk".format(jumlah_produk))

        for i in list(reversed(range(START_DELETE, jumlah_produk+1))):
            tayangan = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH,
                                                                                    "//div[@class=\'cwj9ozl2 ue3kfks5 pw54ja7n uo3d90p7 l82x9zwi o16s864r sej5wr8e m8hsej2k k4urcfbm rnsnyeob\'][{}]/div[@class=\'rq0escxv l9j0dhe7 du4w35lb j83agx80 g5gj957u rj1gh0hx buofh1pr hpfvmrgz i1fnvgqd bp9cbjyn owycx6da btwxx1t3 hv4rvrfc dati1w0a ihqw7lf3 discj3wi b5q2rw42 lq239pai mysgfdmx hddg9phg\']/div[@class=\'rq0escxv l9j0dhe7 du4w35lb j83agx80 cbu4d94t g5gj957u d2edcug0 hpfvmrgz rj1gh0hx buofh1pr p8fzw8mz pcp91wgn qt6c0cv9 jb3vyjys\']/div[@class=\'j83agx80 cbu4d94t i1fnvgqd rvhaf1lv rymlbt9a m706zkia\']/div[@class=\'oajrlxb2 gs1a9yip g5ia77u1 mtkw9kbi tlpljxtp qensuy8j ppp5ayq2 goun2846 ccm00jje s44p3ltw mk2mc5f4 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 rq0escxv nhd2j8a9 j83agx80 mg4g778l btwxx1t3 pfnyh3mw p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x tgvbjcpo hpfvmrgz jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso l9j0dhe7 i1ao9s8h esuyzwwr f1sip0of du4w35lb lzcic4wl abiwlrkh p8dawk7l\']/div[@class=\'d2edcug0\']/div[3]/div[@class=\'tvmbv18p hlyrhctz\']/div[@class=\'j83agx80 cbu4d94t ew0dbk1b irj2b8pg\']/div[@class=\'qzhwtbm6 knvmm38d\']/span[@class=\'d2edcug0 hpfvmrgz qv66sw1b c1et5uql lr9zc1uh a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d9wwppkn fe6kdd0r mau55g9w c8b282yb mdeji52x e9vueds3 j5wam9gi knj5qynh m9osqain hzawbc8m\']/div[@class=\'q9uorilb\']".format(
                                                                                        i)))).get_attribute("innerText")
            tayangan = re.findall(r'\d+', tayangan)[0]
            judul = WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH,
                                                                               "//div[@class=\'cwj9ozl2 ue3kfks5 pw54ja7n uo3d90p7 l82x9zwi o16s864r sej5wr8e m8hsej2k k4urcfbm rnsnyeob\'][{}]/div[@class=\'rq0escxv l9j0dhe7 du4w35lb j83agx80 g5gj957u rj1gh0hx buofh1pr hpfvmrgz i1fnvgqd bp9cbjyn owycx6da btwxx1t3 hv4rvrfc dati1w0a ihqw7lf3 discj3wi b5q2rw42 lq239pai mysgfdmx hddg9phg\']/div[@class=\'rq0escxv l9j0dhe7 du4w35lb j83agx80 cbu4d94t g5gj957u d2edcug0 hpfvmrgz rj1gh0hx buofh1pr p8fzw8mz pcp91wgn qt6c0cv9 jb3vyjys\']/div[@class=\'j83agx80 cbu4d94t i1fnvgqd rvhaf1lv rymlbt9a m706zkia\']/div[@class=\'oajrlxb2 gs1a9yip g5ia77u1 mtkw9kbi tlpljxtp qensuy8j ppp5ayq2 goun2846 ccm00jje s44p3ltw mk2mc5f4 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 rq0escxv nhd2j8a9 j83agx80 mg4g778l btwxx1t3 pfnyh3mw p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x tgvbjcpo hpfvmrgz jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso l9j0dhe7 i1ao9s8h esuyzwwr f1sip0of du4w35lb lzcic4wl abiwlrkh p8dawk7l\']/div[@class=\'d2edcug0\']/div[@class=\'tvmbv18p\']/div[@class=\'j83agx80 cbu4d94t ew0dbk1b irj2b8pg\']/div[@class=\'qzhwtbm6 knvmm38d\'][1]/span[@class=\'d2edcug0 hpfvmrgz qv66sw1b c1et5uql lr9zc1uh a8c37x1j keod5gw0 nxhoafnm aigsh9s9 d3f4x2em fe6kdd0r mau55g9w c8b282yb iv3no6db a5q79mjw g1cxx5fr lrazzd5p oo9gr5id hzawbc8m\']/span[@class=\'a8c37x1j ni8dbmo4 stjgntxs l9j0dhe7 ojkyduve\']".format(
                                                                                   i)))).get_attribute("innerText")
            tulis_log("Cek produk ke {} | {} | tayangan = {}".format(i,judul, tayangan))

            if int(tayangan) < VIEW_MINIMAL_DELETE+1:
                tulis_log("hapus")
                c = 2 * i - 1

                driver.execute_script("arguments[0].click();", WebDriverWait(driver, 20).until(
                    EC.element_to_be_clickable((By.XPATH, "(//div[@aria-label='Lainnya']/div)[{}]".format(c)))))
                driver.execute_script("arguments[0].click();", WebDriverWait(driver, 20).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[@class='qzhwtbm6 knvmm38d' and contains(.,'Hapus Tawaran')]/span"))))

                try:
                    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 5).until(EC.element_to_be_clickable((
                                                                                                                             By.XPATH,
                                                                                                                             "//div[@class='sjgh65i0']/div[@class='rq0escxv l9j0dhe7 du4w35lb j83agx80 pfnyh3mw jifvfom9 gs1a9yip owycx6da rl25f0pe d1544ag0 tw6a2znq discj3wi dlv3wnog rl04r1d5 enqfppq2 muag1w35']/div[@class='rq0escxv l9j0dhe7 du4w35lb j83agx80 cbu4d94t pfnyh3mw d2edcug0 hpfvmrgz ph5uu5jm b3onmgus e5nlhep0 ecm0bbzt mg4g778l'][1]/div[@class='oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 pq6dq46d p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l cbu4d94t taijpn5t k4urcfbm']/div[@class='rq0escxv l9j0dhe7 du4w35lb j83agx80 pfnyh3mw taijpn5t bp9cbjyn owycx6da btwxx1t3 kt9q3ron ak7q8e6j isp2s0ed ri5dt5u2 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 d1544ag0 tw6a2znq s1i5eluu tv7at329']"))))
                except Exception as e:
                    driver.execute_script("arguments[0].click();", WebDriverWait(driver, 5).until(EC.element_to_be_clickable((
                                                                                                                             By.XPATH,
                                                                                                                             "//div[@class='oajrlxb2 g5ia77u1 qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 pq6dq46d p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l cbu4d94t taijpn5t k4urcfbm']/div[@class='rq0escxv l9j0dhe7 du4w35lb j83agx80 pfnyh3mw taijpn5t bp9cbjyn owycx6da btwxx1t3 kt9q3ron ak7q8e6j isp2s0ed ri5dt5u2 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 d1544ag0 tw6a2znq s1i5eluu tv7at329']"))))
                sleep(2)

                try:
                    WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH,
                                                                               "//div[@class=\'cypi58rs pmk7jnqg fcg2cn6m tkr6xdv7\']/div[@class=\'oajrlxb2 tdjehn4e qu0x051f esr5mh6w e9989ue4 r7d6kgcz rq0escxv nhd2j8a9 j83agx80 p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso i1ao9s8h esuyzwwr f1sip0of lzcic4wl l9j0dhe7 abiwlrkh p8dawk7l bp9cbjyn s45kfl79 emlxlaya bkmhp75w spb7xbtv rt8b4zig n8ej3o3l agehan2d sk4xxmp2 taijpn5t tv7at329 thwo4zme\']"))).click()
                except Exception as y:
                    sleep(1)

            else:
                a = 0

        driver.close()
        tulis_log("Berhasil hapus produk akun {} -> {}".format(kodeakun, nama))
    except Exception as e:
        tulis_log("Error saat proses ke akun {} -> {}".format(kodeakun, nama))
        print(e)
        driver.close()