import json
import logging
import os
import re
import requests
import sys
import time
import sqlite3
import random

import urllib.parse
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.file_detector import LocalFileDetector
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.wait import WebDriverWait
from time import sleep
from typing import Dict, List

""" CONFIG """

#setting file config
path_config = "config.json"
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")


CHROMEDRIVER = config['setting'][0]['chromedriver']
ULANG = config['setting'][0]["jumlah_perulangan"]
JEDA = config['setting'][0]["jeda"]
AKUN = config['setting'][0]["akun"]
TOKEN = config['setting'][0]["telegram_bot_token"]
CHATID = config['setting'][0]["telegram_chat_id"]
FILE_DOWNLOAD = config['scrape_produk'][0]["file"]
FILE_DOWNLOAD_SEPARATOR = config['scrape_produk'][0]["separator"]
FILE_UPLOAD = config['update_produk'][0]["file"]
FILE_UPLOAD_SEPARATOR = config['update_produk'][0]["separator"]
START_UPDATE = config['update_produk'][0]["start"]
JEDA_UPDATE = config['update_produk'][0]["jeda"]
FILE_UPLOAD_PRODUK = config['upload_produk'][0]["file"]
START_UPLOAD_PRODUK = config['upload_produk'][0]["start"]
JEDA_UPLOAD_PRODUK = config['upload_produk'][0]["jeda"]
FILE_DELETE = config['delete_produk'][0]["file"]
FILE_OPTIMASI_PESAN = config['optimasi_pesan'][0]["file"]
FILE_OPTIMASI_SEPARATOR = config['update_produk'][0]["separator"]
START_OPTIMASI_PESAN = config['optimasi_pesan'][0]["start"]
JEDA_OPTIMASI_PESAN = config['optimasi_pesan'][0]["jeda"]
RANDOM_PESAN = config['optimasi_pesan'][0]["pesan"]

""" FUNGSI TULIS"""
def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)
    
def simpan(text):
    f = open("scrape_produk.csv", "a")
    f.write(text+"\n")
    f.close()

""" Login module """

def domain_to_url(domain: str) -> str:
    """ Converts a (partial) domain to valid URL """
    if domain.startswith("."):
        domain = "www" + domain
    return "http://" + domain

def login_using_cookie_file(driver: WebDriver, cookie_file: str):
    """Restore auth cookies from a file. Does not guarantee that the user is logged in afterwards.
    Visits the domains specified in the cookies to set them, the previous page is not restored."""
    domain_cookies: Dict[str, List[object]] = {}
    with open(cookie_file) as file:
        cookies: List = json.load(file)
        # Sort cookies by domain, because we need to visit to domain to add cookies
        for cookie in cookies:
            try:
                domain_cookies[cookie["domain"]].append(cookie)
            except KeyError:
                domain_cookies[cookie["domain"]] = [cookie]

    for domain, cookies in domain_cookies.items():
        driver.get(domain_to_url(domain + "/robots.txt"))
        for cookie in cookies:
            cookie.pop("sameSite", None)  # Attribute should be available in Selenium >4
            cookie.pop("storeId", None)  # Firefox container attribute
            try:
                driver.add_cookie(cookie)
            except:
                tulis_log(f"Couldn't set cookie {cookie['name']} for {domain}")

# kodeakun = "#ZX001"
# cookies = "F:\FB_MP\Cookies FB\#ZX001.json"
# nama = "Claudia Audy"

f = open(FILE_OPTIMASI_PESAN, "r")
a = f.readlines()[START_OPTIMASI_PESAN:]
f.close()

count = START_OPTIMASI_PESAN
for line in a:
    data = line.strip().split(FILE_UPLOAD_SEPARATOR)
    cookies = data[0]
    url = data[1]
    id_produk = re.findall(r'\d+', url)[0]
    
    tulis_log("Proses optimasi pesan ke {}".format(count))

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    # driver = webdriver.Chrome(CHROMEDRIVER,options=chrome_options)
    driver = webdriver.Chrome(CHROMEDRIVER)
    
    try:
        tulis_log("Login ke akun -> {}".format(cookies))
        login_using_cookie_file(driver,cookies)
        driver.get("https://mobile.facebook.com")
        sleep(5)
        tulis_log("Membuka halaman produk")
        driver.get("https://mobile.facebook.com/marketplace/item/{}".format(id_produk))
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//input[@type=\'text\']"))).click()
        sleep(1)
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//input[@type=\'text\']"))).send_keys(Keys.CONTROL + "a")
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//input[@type=\'text\']"))).send_keys(Keys.DELETE)
        sleep(1)
        psn = random.choice(RANDOM_PESAN)
        tulis_log("Kirim pesan : {}".format(psn))
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//input[@type=\'text\']"))).send_keys(psn)
        sleep(1)
        # WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//input[@type=\'text\']"))).send_keys(Keys.ENTER)
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//button"))).click()
        sleep(5)
        sleep(random.randrange(JEDA_OPTIMASI_PESAN-5,JEDA_OPTIMASI_PESAN+5))
        driver.close()
    except Exception as e:
        tulis_log("Error saat proses ke {}".format(count))
        tulis_log(e)
        driver.close()
    
    count += 1

tulis_log("Done !!!")