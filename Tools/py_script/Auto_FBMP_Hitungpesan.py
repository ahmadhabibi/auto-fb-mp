import json
import logging
import os
import re
import requests
import sys
import time
import sqlite3

import urllib.parse
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.file_detector import LocalFileDetector
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.wait import WebDriverWait
from time import sleep
from typing import Dict, List

""" CONFIG """

#setting file config
path_config = "config.json"
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

CHROMEDRIVER = config['setting'][0]['chromedriver']
ULANG = config['setting'][0]["jumlah_perulangan"]
JEDA = config['setting'][0]["jeda"]
AKUN = config['setting'][0]["akun"]
TOKEN = config['setting'][0]["telegram_bot_token"]
CHATID = config['setting'][0]["telegram_chat_id"]
FILE_DOWNLOAD = config['scrape_produk'][0]["file"]
FILE_DOWNLOAD_SEPARATOR = config['scrape_produk'][0]["separator"]
FILE_UPLOAD = config['update_produk'][0]["file"]
FILE_UPLOAD_SEPARATOR = config['update_produk'][0]["separator"]
START_UPDATE = config['update_produk'][0]["start"]
JEDA_UPDATE = config['update_produk'][0]["jeda"]
FILE_UPLOAD_PRODUK = config['upload_produk'][0]["file"]
START_UPLOAD_PRODUK = config['upload_produk'][0]["start"]
JEDA_UPLOAD_PRODUK = config['upload_produk'][0]["jeda"]
FILE_DELETE = config['delete_produk'][0]["file"]
FILE_OPTIMASI_PESAN = config['optimasi_pesan'][0]["file"]
FILE_OPTIMASI_SEPARATOR = config['update_produk'][0]["separator"]
START_OPTIMASI_PESAN = config['optimasi_pesan'][0]["start"]
JEDA_OPTIMASI_PESAN = config['optimasi_pesan'][0]["jeda"]
RANDOM_PESAN = config['optimasi_pesan'][0]["pesan"]

""" FUNGSI TULIS"""
def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)

""" Telegram Send Message """

def susun_text(data):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    text = "**Laporan Pesan masuk pada tanggal {}**\n".format(dt_string)
    for i in data:
        dt = i.split(";")
        kode_akun = dt[0]
        nama_akun = dt[1]
        jumlah_pesan = dt[2]
        new_line = "{} | {} -> {} Pesan Baru".format(kode_akun,nama_akun,jumlah_pesan)
        text = "{}\n{}".format(text,new_line)
    return text.strip()

def telegram_bot_sendtext(bot_message):
    bot_token = TOKEN
    bot_chatID = CHATID
    send_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_chatID + '&parse_mode=Markdown&text=' + bot_message
    response = requests.get(send_text)
    res = response.json()
    return res['ok']

""" Login module """

def domain_to_url(domain: str) -> str:
    """ Converts a (partial) domain to valid URL """
    if domain.startswith("."):
        domain = "www" + domain
    return "http://" + domain

def login_using_cookie_file(driver: WebDriver, cookie_file: str):
    """Restore auth cookies from a file. Does not guarantee that the user is logged in afterwards.
    Visits the domains specified in the cookies to set them, the previous page is not restored."""
    domain_cookies: Dict[str, List[object]] = {}
    with open(cookie_file) as file:
        cookies: List = json.load(file)
        # Sort cookies by domain, because we need to visit to domain to add cookies
        for cookie in cookies:
            try:
                domain_cookies[cookie["domain"]].append(cookie)
            except KeyError:
                domain_cookies[cookie["domain"]] = [cookie]

    for domain, cookies in domain_cookies.items():
        driver.get(domain_to_url(domain + "/robots.txt"))
        for cookie in cookies:
            cookie.pop("sameSite", None)  # Attribute should be available in Selenium >4
            cookie.pop("storeId", None)  # Firefox container attribute
            try:
                driver.add_cookie(cookie)
            except:
                tulis_log(f"Couldn't set cookie {cookie['name']} for {domain}")

""" Fungsi Cek Pesan FB """
def tombol_lanjut():
    try:
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//a[contains(.,\'Lihat Pesan Sebelumnya\')]"))).click()
        sleep(2)
        tombol_lanjut()
    except:
        time.sleep(1)
        
def hitung_pesan(driver: WebDriver,set_cookies):
    login_using_cookie_file(driver,set_cookies)
    driver.get("https://mobile.facebook.com")
    driver.get("https://mobile.facebook.com/messages/?folder=unread")
    tombol_lanjut()
    pm = driver.find_elements(By.XPATH, "//a[contains(@href, \'/messages/read/\')]")
    return(len(pm))
 
""" MAIN LOOPING """
for i in range(1,ULANG):
    tulis_log("Proses Cek ke-{}".format(i))
    tidakada_pesan = []
    ada_pesan = []

    f = open(AKUN, "r")
    a = f.readlines()[1:]
    f.close()

    count = 0
    for line in a:
        count += 1
        data = line.strip().split(";")
        kodeakun = data[0]
        cookies = data[1]
        nama = data[2]
        tulis_log("Proses cek pesan akun {} | {}".format(kodeakun,nama))
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-software-rasterizer")
        driver = webdriver.Chrome(CHROMEDRIVER,options=chrome_options)
        try:
            jumlah_pesan = hitung_pesan(driver,cookies)
            if jumlah_pesan != 0:
                tulis_log("Ada {} pesan yang belum terbaca".format(jumlah_pesan))
                dt = "{};{};{}".format(kodeakun,nama,jumlah_pesan)
                ada_pesan.append(dt)
            else:
                tulis_log("Tidak ada pesan baru")
                dt = "{};{};{}".format(kodeakun,nama,jumlah_pesan)
                tidakada_pesan.append(dt)
            driver.close()
        except Exception as e:
            tulis_log("Error saat membuka akun ke {} | {} -> {}".format(count,kodeakun,nama))
            tulis_log(e)

    if len(ada_pesan) > 0:
        tulis_log("Kirim laporan ke Telegram")
        text = susun_text(ada_pesan)
        if telegram_bot_sendtext(urllib.parse.quote_plus(text)) == True:
            tulis_log("Sukses kirim laporan ke Telegram")
        else:
            tulis_log("Gagal kirim laporan ke Telegram")
    tulis_log("Jeda {} Detik\n".format(JEDA))
    sleep(JEDA)

telegram_bot_sendtext("Selesai cek")