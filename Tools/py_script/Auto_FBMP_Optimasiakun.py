import json
import logging
import os
import re
import requests
import sys
import time
import sqlite3
import random

import urllib.parse
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.file_detector import LocalFileDetector
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.wait import WebDriverWait
from time import sleep
from typing import Dict, List

""" CONFIG """

#setting file config
path_config = "config.json"
if os.path.isfile(path_config):
    with open(path_config, 'r') as f:
        config = json.load(f)
else:
    sys.exit("File config.json tidak ada, silahkan setting terlebih dahulu !!!")

CHROMEDRIVER = config['setting'][0]['chromedriver']
AKUN = config['setting'][0]["akun"]
RANDOM_PESAN = config['optimasi_pesan'][0]["pesan"]
FILE_STATUS = config['optimasi_akun'][0]["file_status"]
JUMLAH_LIKE = config['optimasi_akun'][0]["jumlah_like"]
JUMLAH_ADD = config['optimasi_akun'][0]["jumlah_add"]

""" FUNGSI TULIS"""
def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)
    
def simpan(text):
    f = open("scrape_produk.csv", "a")
    f.write(text+"\n")
    f.close()

""" Login module """

def domain_to_url(domain: str) -> str:
    """ Converts a (partial) domain to valid URL """
    if domain.startswith("."):
        domain = "www" + domain
    return "http://" + domain

def login_using_cookie_file(driver: WebDriver, cookie_file: str):
    """Restore auth cookies from a file. Does not guarantee that the user is logged in afterwards.
    Visits the domains specified in the cookies to set them, the previous page is not restored."""
    domain_cookies: Dict[str, List[object]] = {}
    with open(cookie_file) as file:
        cookies: List = json.load(file)
        # Sort cookies by domain, because we need to visit to domain to add cookies
        for cookie in cookies:
            try:
                domain_cookies[cookie["domain"]].append(cookie)
            except KeyError:
                domain_cookies[cookie["domain"]] = [cookie]

    for domain, cookies in domain_cookies.items():
        driver.get(domain_to_url(domain + "/robots.txt"))
        for cookie in cookies:
            cookie.pop("sameSite", None)  # Attribute should be available in Selenium >4
            cookie.pop("storeId", None)  # Firefox container attribute
            try:
                driver.add_cookie(cookie)
            except:
                tulis_log(f"Couldn't set cookie {cookie['name']} for {domain}")

def scroll(i):
    driver.execute_script("window.scrollTo(0, document.getElementById('viewport').scrollHeight);")
    sleep(2)

f = open(FILE_STATUS, "r")
status_file = f.readlines()
f.close()

f = open(AKUN, "r")
akuns = f.readlines()[1:]
f.close()

count = 0
for akun in akuns:
    count += 1
    data = akun.strip().split(";")
    kodeakun = data[0]
    cookies = data[1]
    nama = data[2]
    
    tulis_log("Proses optimasi akun {} | {}".format(kodeakun,nama))
    driver = webdriver.Chrome(CHROMEDRIVER)
    login_using_cookie_file(driver,cookies)
    driver.get("https://mobile.facebook.com")

    try:
        r = random.randrange(1,len(status_file))
        kata_line = status_file[r].split(";")
        kata = kata_line[0]
        author = kata_line[1]
        status = "{} - {} ".format(kata,author)
        
        tulis_log("Update status")

        driver.get("https://mobile.facebook.com")
        sleep(2)
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//div[@id='MComposer']/div/div/div[1]"))).click()
        warna =random.randrange(2,7)
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//form[@id='structured_composer_form']/div[3]/div/div[{}]".format(warna)))).click()
        sleep(2)

        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//textarea[@class='composerInput mentions-input']"))).click()
        WebDriverWait(driver, 5).until(EC.element_to_be_clickable((By.XPATH, "//textarea[@class='composerInput mentions-input']"))).send_keys(status)
        sleep(2)
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//div[@id='structured_composer']/div[@id='composer-main-view-id']/div/div/div/button"))).click()
        sleep(10)
        
        tulis_log("Like status beranda")
        driver.get("https://mobile.facebook.com")
        sleep(2)
        
        for i in range(1,JUMLAH_LIKE):
            scroll(i)

        id_suka = []
        jml = 0
        ids_suka = driver.find_elements(By.XPATH, "//a[contains(@data-sigil,\'like\')]")
        for a in ids_suka:
            status = a.get_attribute('aria-pressed')
            if (status == "false") and (jml < JUMLAH_LIKE):
                id_suka.append(a.get_attribute('id'))
                WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.ID, a.get_attribute('id')))).click()
                jml = jml+1
                driver.execute_script("window.scrollTo(0,1000)")
                sleep(5)
        
        tulis_log("Add friends")
        driver.get("https://mobile.facebook.com/friends/center/suggestions/?_rdr")
        sleep(5)
        
        for i in range(1,JUMLAH_ADD+1):
            element = driver.find_element(By.XPATH, "(//a[contains(@data-sigil,'m-add-friend')]/button)[{}]".format(i))
            actions = ActionChains(driver)
            actions.move_to_element(element).perform()
            driver.find_element(By.XPATH, "(//a[contains(@data-sigil,'m-add-friend')]/button)[{}]".format(i)).click()
            driver.execute_script("window.scrollTo(0,303)")
            sleep(3)
        
        tulis_log("Optimasi berhasil untuk akun {} | {}".format(kodeakun,nama))
        driver.close()
    except Exception as e:
        tulis_log("Proses error untuk akun {} | {}".format(kodeakun,nama))
        tulis_log(e)
        driver.close()

tulis_log("Done")