import time
from datetime import datetime

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.remote.file_detector import LocalFileDetector
from selenium.webdriver.chrome.options import Options

""" FUNGSI TULIS"""
def tulis_log(text):
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    tulisan = "{} | {}".format(dt_string,text)
    f = open("riwayat.log", "a")
    f.write(tulisan+"\n")
    f.close()
    print(tulisan)

def tulis_link_video(txt):
    f = open("link_channel.csv", "a")
    f.write(txt+"\n")
    f.close()

def jumlah_video(url):
    driver.get(url)
    time.sleep(1)
    
    tulis_log("Ambil data video")
    elem = driver.find_element_by_tag_name("body")
    lastHeight = driver.execute_script("return document.getElementById('content').scrollHeight")
    while True:
      driver.execute_script("window.scrollTo(0, document.getElementById('content').scrollHeight);")
      time.sleep(3)
      newHeight = driver.execute_script("return document.getElementById('content').scrollHeight")
      if newHeight == lastHeight:
        break
      lastHeight = newHeight
    WebDriverWait(driver, 100).until(EC.element_to_be_clickable((By.XPATH,"//a[@id='video-title']")))
    
    video=driver.find_elements(By.XPATH,"//a[@id='video-title']")
    tulis_log("Berhasil mengambil data video")
    tulis_log("Ada {} video di channel ini".format(len(video)))
    for i in video:
        judul = i.get_attribute('innerText')
        link = i.get_attribute('href')
        tulis_link_video("{};{}".format(link,judul))
    tulis_log("Link video & judul ada di file link_channel.csv")
    tulis_log("===============")

f = open("list_channel.txt", "r")
a = f.read()
f.close()

tulis_log("Youtube Video Scraper by Habibi")
tulis_log("================================================")
time.sleep(5)
for line in a.split("\n"):
    driver = webdriver.Chrome("chromedriver.exe")
    tulis_log("Buka channel {}".format(line))
    jumlah_video(line)
    driver.close()

tulis_log("Done !!!")
